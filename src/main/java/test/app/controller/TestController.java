package test.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	@RequestMapping(value = "/")
	public String test() {
		System.out.println("Hello world!!");
		return "index";
	}
}